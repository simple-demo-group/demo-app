import './App.css';

export const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Demo App</h1>
        <h2>Built on Thursday October 23rd 2023 🕹️</h2>
      </header>
    </div>
  );
}